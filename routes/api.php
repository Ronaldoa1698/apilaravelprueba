<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('categoria', 'App\Http\Controllers\categoriacontroller@getCategoria');
Route::get('categoria/{id}', 'App\Http\Controllers\categoriacontroller@getCategoriaxid');
Route::post('addcategoria', 'App\Http\Controllers\categoriacontroller@insertCategoria');
Route::put('updatecategoria/{id}', 'App\Http\Controllers\categoriacontroller@updateCategoria');
Route::delete('deletecategoria/{id}', 'App\Http\Controllers\categoriacontroller@deleteCategoria');